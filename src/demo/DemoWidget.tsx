import React, {Component} from 'react'
import {render} from 'react-dom'
import {autorun, observable, toJS} from 'mobx'
import {observer} from 'mobx-react'
import {map, reduce, values, cloneDeep} from 'lodash'
import {
  AllEpikQueryParams,
  CreditCardIframeMessageType,
  CustomerInfo,
  Epik,
  EpikEvent,
  EpikEventName,
  EpmsPaymentInfo,
  EpmsSubscriptionInfo,
  EpmsPaymentSourceInfo,
  isCreditCardPaymentMethod,
  Payment,
  PaymentConfig,
  PaymentCurrency,
  PaymentErrors,
  PaymentFlow,
  PaymentMethod,
  PaymentProvider,
  RecurringInterval,
  SubscriptionInfo,
  TransactionInfo,
  TransactionStatus,
} from '@raisenow/epik'
import {Checkbox, Input, Select, SelectOption} from './FormBaseComponents'
import {AddressFieldsView, CreditCardIframeView, DebugSection, PaymentSpecificFieldsView} from './FormComponents'

///////////////////////////////////////////////////////////////////////////////

export type WidgetConfig = WidgetProps

export type WidgetProps = {
  epik: Epik
  title: string
}

export type WidgetContainer = {
  node: JSX.Element
}

export type WidgetData = Partial<PaymentConfig>

export type WidgetFlags = {
  credit_card_iframe: boolean
  skip_cvv: boolean
  sending: boolean
}

export type WidgetOptions = {
  flow: SelectOption[]
  payment_method: SelectOption[]
  payment_provider: SelectOption[]
  profile: SelectOption[]
  recurring_interval: SelectOption[]
  currency: SelectOption[]
  language: SelectOption[]
  country: SelectOption[]
  payment_slip_delivery_types: SelectOption[]
}

///////////////////////////////////////////////////////////////////////////////

@observer export class Widget extends Component<WidgetProps> {
  private epik: Epik
  private payment: Payment
  @observable private data: WidgetData
  @observable private flags: WidgetFlags
  @observable private validationErrors: PaymentErrors
  @observable private transactionInfo: TransactionInfo | {[key: string]: any}
  @observable private subscriptionInfo: SubscriptionInfo | {[key: string]: any}
  @observable private customerInfo: CustomerInfo | {[key: string]: any}
  @observable private epmsPaymentInfo: EpmsPaymentInfo | {[key: string]: any}
  @observable private epmsSubscriptionInfo: EpmsSubscriptionInfo | {[key: string]: any}
  @observable private epmsPaymentSourceInfo: EpmsPaymentSourceInfo | {[key: string]: any}
  @observable private epmsPaymentResponseData: any
  private options: WidgetOptions

  constructor(props) {
    super(props)
    this.init(props)
  }

  private init(props) {
    this.epik = props.epik
    this.payment = this.epik.createPayment()
    this.submitForm = this.submitForm.bind(this)
    this.resetForm = this.resetForm.bind(this)
    this.setDefaultData()
    this.initEventsHandling()
    this.epik.init()

    autorun(() => {
      // rerun on data change
      this.data

      this.payment.update(toJS(this.data))
    })

    autorun(() => {
      // rerun on "this.flags.skip_cvv" changes
      this.epik.config.skipCvv = this.flags.skip_cvv
    })

    autorun(() => {
      // rerun on "this.data.language" changes
      this.epik.config.language = this.data.language

      this.payment.creditCardIframe?.messenger.emit(CreditCardIframeMessageType.SET_LANGUAGE, this.epik.config.language)
    })

    autorun(() => {
      // rerun on "this.data.payment_method" changes
      this.data.payment_method

      // rerun on "this.flags.skip_cvv" changes
      this.flags.skip_cvv

      // rerun on "this.flags.credit_card_iframe" changes
      this.flags.credit_card_iframe

      // delay here to allow payment to be updated first
      setTimeout(async () => {
        if (isCreditCardPaymentMethod(this.data.payment_method) && this.flags.credit_card_iframe) {
          await this.payment.showCreditCardIframe()
          this.payment.creditCardIframe?.messenger.on(CreditCardIframeMessageType.SUBMIT, () => this.submitForm())
        } else {
          this.payment.hideCreditCardIframe()
        }
      }, 0)
    })

    autorun(() => {
      // rerun on "this.data.flow" changes
      this.data.flow

      // delay here to allow payment to be updated first
      setTimeout(async () => {
        this.payment.creditCardIframe?.messenger.emit(CreditCardIframeMessageType.SET_FLOW, this.data.flow)
      }, 0)
    })

    autorun(() => {
      // rerun on "this.data.payment_method" changes
      this.data.payment_method

      const payment = cloneDeep(this.payment.read())

      if (this.data.payment_method === PaymentMethod.TWINT) {
        payment.payment_information = {
          return_scheme: 'twint-issuer-1://',
          return_package: 'intent://payment#Intent;package=ch.twint.payment;action=ch.twint.action.TWINT_PAYMENT;scheme=twint;S.code=12345;S.startingOrigin=SMALL_BUSINESS_SOLUTION;end',
        }
      } else {
        payment.payment_information = {}
      }

      this.payment.replace(payment)
    })
  }

  private setDefaultData(): void {
    this.flags = {
      credit_card_iframe: false,
      skip_cvv: false,
      sending: false,
    }
    this.data = {
      // epms data
      // profile: 'stripe_card-54d', //'payu_arg_moto', // ''
      // profile: 'payu_chl_moto', // ''
      profile: '',
      // return_url: window.location.href,
      // return_url: 'http://0.0.0.0:8080/?aaa=AAA#qwe',
      create_supporter: false,
      supporter: {
        metadata: {
          foo: 'initial Foo',
        },
      },
      // raisenow: {
      //   solution: {
      //     name: 'test-solution',
      //     uuid: '123-456-789',
      //   },
      //   product: {
      //     name: 'initial name',
      //     version: 'initial version',
      //     uuid: 'initial uuid',
      //     source_url: 'initial source_url',
      //   },
      // },
      custom_parameters: {
        foo: 'initial Foo',
      },

      test_mode: true,
      return_parameters: false,
      language: 'en',
      recurring: false,
      stored_pseudo_recurring: false,

      // flow: PaymentFlow.EPP,
      flow: PaymentFlow.EPMS,

      recurring_interval: RecurringInterval.MONTHLY,

      // payment_method: PaymentMethod.CREDIT_CARD,
      payment_method: PaymentMethod.TWINT,
      // payment_method: PaymentMethod.PAYMENT_SLIP_WITH_REFERENCE_NUMBER,

      payment_provider: PaymentProvider.DATATRANS,
      // payment_provider: PaymentProvider.PAYU,
      // payment_provider: PaymentProvider.STRIPE,

      amount: 1000,
      // currency: PaymentCurrency.CHF,
      currency: PaymentCurrency.USD,
      // currency: 'ars' as PaymentCurrency,
      // currency: 'clp' as PaymentCurrency,

      stored_customer_firstname: 'John',
      stored_customer_lastname: 'Doe',
      stored_customer_email: 'sasha@bytesoft.io',
      stored_customer_birthdate: '1986-08-26',

      stored_customer_street: 'Main Rd.',
      stored_customer_street_number: '100',
      stored_customer_zip_code: '10022', // 1337

      // stored_customer_city: 'Zurich',
      // stored_customer_city: 'Buenos Aires',
      stored_customer_city: 'Santiago',

      // stored_customer_country: 'CH',
      stored_customer_country: 'AR',
      // stored_customer_country: 'CL',

      stored_customer_email_permission: true,
      stored_customer_message: 'some message',

      cardno: '4242424242424242',

      // cardno: '5895620000000002',   // naranja
      // cardno: '6034880000000051',   // shopping
      // cardno: '5896570000000008',   // cabal
      // cardno: '5011050000000001',   // argencard
      // cardno: '6034930000000005',   // cencosud
      // cardno: '6062820000000003',   // hipercard
      // cardno: '5907120000000009',   // codensa
      // cardno: '5412030000000008',   // credencial

      cvv: '123',
      expm: '12',
      expy: '21',

      iban: 'CH9300762011623852957',
      bank_name: 'UBS',
      bank_zip_code: '8005',
      bank_city: 'Zürich',
      bankiban: 'CH9300762011623852957',
      msisdn: '+41788799186',

      // payu
      statement_descriptor: 'Text for card statement',

      stored_customer_document_type: 'xxx1',
      stored_customer_document_number: 'xxx2',
      stored_customer_company_identification_number: 'xxx3',
      stored_customer_state: 'Buenos Aires',
      // stored_customer_state: 'Santiago',
      stored_customer_phone: '123-456-789',

      // specific for direct debit
      form_template: 'https://assets.raisenow.com/static/1234567890/LEMA_LSV_Template_2017_DE_PostFinance.pdf',

      // specific for payment slip
      stored_es_ordered: false,
      stored_ezs_ordered: false,

      // custom fields
      stored_campaign_id: 'xxx',
      // stored_campaign_subid: 'yyy',

      stored_rnw_product_name: 'some product name',
      stored_rnw_product_version: 'some product version',
      // stored_rnw_widget_uuid: 'some uuid',
      // stored_rnw_source_url: 'some source_url',

      // ignored, as supporter.metadata.foo is already set
      stored_customer_foo: 'Foo',

      // sets supporter.metadata.bar
      stored_customer_bar: 'Bar',

      // sets raisenow.integration.donation_receipt_requested
      stored_customer_donation_receipt: false,

      // ignored, as custom_parameters.foo is already set
      stored_foo: 'Foo',

      // sets custom_parameters.foo
      stored_bar: 'Bar',

      stored_rnw_analytics_preselected_amount: 500,
      stored_rnw_analytics_suggested_amounts: "[500,1000,3000,12000]",
      stored_rnw_cover_fee_fixed: 200,
      stored_rnw_cover_fee_percentage: 2,
      stored_rnw_cover_fee_amount: 300,
      stored_cover_transaction_fee: false,
    }

    this.options = {
      flow: map(values(PaymentFlow), v => ({value: v, label: v})),
      payment_method: [
        {value: PaymentMethod.CREDIT_CARD, label: 'cc - Credit card'},
        {value: PaymentMethod.AMERICAN_EXPRESS, label: 'amx - American express'},
        {value: PaymentMethod.CYMBA, label: 'cym - Cymba (GB only)'},
        {value: PaymentMethod.DINERS_CLUB, label: 'din - Diners club'},
        {value: PaymentMethod.DIRECT_DEBIT, label: 'dd - Direct Debit'},
        {value: PaymentMethod.DISCOVERY, label: 'dis - Discovery'},
        {value: PaymentMethod.WEBBANKING_INDIA, label: 'ebs - EBS (India only)'},
        {value: PaymentMethod.ELEKTRONISCHES_LASTSCHRIFTVERFAHREN, label: 'elv - Elektronisches Lastschriftverfahren'},
        {value: PaymentMethod.ELECTRONIC_PAYMENT_SERVICE, label: 'eps - EPS (Austria only)'},
        {value: PaymentMethod.JAPANESE_CREDIT_BUREAU, label: 'jcb - JAPANESE_CREDIT_BUREAU'},
        {value: PaymentMethod.MAESTRO, label: 'mae - Maestro'},
        {value: PaymentMethod.MASTER_CARD, label: 'eca - Master card'},
        {value: PaymentMethod.MASTER_PASS, label: 'mpw - MasterPass'},
        {value: PaymentMethod.PAYMENT_SLIP, label: 'es - Payment Slip'},
        {value: PaymentMethod.PAYMENT_SLIP_WITH_REFERENCE_NUMBER, label: 'ezs - Payment Slip+RefNo (Swiss only)'},
        {value: PaymentMethod.PAY_PAL, label: 'pp - PayPal'},
        {value: PaymentMethod.POST_FINANCE, label: 'pef - PostFinance E-Finance (Swiss only)'},
        {value: PaymentMethod.POST_FINANCE_CARD, label: 'pfc - PostFinance Card (Swiss only)'},
        {value: PaymentMethod.SEPA_ONE, label: 'sod - SEPAone'},
        {value: PaymentMethod.SMS, label: 'sms - SMS (Swiss only)'},
        {value: PaymentMethod.SOFORT, label: 'dib - Sofort'},
        {value: PaymentMethod.TWINT, label: 'twi - Twint (CH only)'},
        {value: PaymentMethod.VISA, label: 'vis - Visa'},
        {value: PaymentMethod.MPOS, label: 'mpos - RaiseNow Terminal'},
      ],
      payment_provider: map(values(PaymentProvider), v => ({value: v, label: v})),
      profile: map(['', 'stripe_card-54d', 'payu_arg_moto', 'payu_chl_moto'], v => ({value: v, label: v})),
      recurring_interval: map(values(RecurringInterval), v => ({value: v, label: v})),
      currency: map([...values(PaymentCurrency), 'ars', 'clp'], v => ({value: v, label: v})),
      language: [
        {value: 'en', label: 'English'},
        {value: 'de', label: 'German'},
        {value: 'fr', label: 'French'},
        {value: 'it', label: 'Italian'},
      ],
      country: [
        {value: 'CH', label: 'Switzerland'},
        {value: 'DE', label: 'Germany'},
        {value: 'FR', label: 'France'},
        {value: 'IT', label: 'Italy'},
        {value: 'AR', label: 'Argentina'},
        {value: 'CL', label: 'Chile'},
      ],
      payment_slip_delivery_types: [
        {value: false, label: 'Download'},
        {value: true, label: 'Mail'},
      ]
    }
  }

  private initEventsHandling() {
    this.epik.events[EpikEventName.FETCH_PAYMENT_DATA_END].subscribe((event: EpikEvent) => {
      let {
        transactionInfo,
        subscriptionInfo,
        customerInfo,
        epmsPaymentInfo,
        epmsSubscriptionInfo,
        epmsPaymentSourceInfo,
        transactionError,
        subscriptionError,
        customerError,
        epmsPaymentError,
        epmsSubscriptionError,
        epmsPaymentSourceError,
      } = event.data

      this.transactionInfo = transactionInfo
      this.subscriptionInfo = subscriptionInfo
      this.customerInfo = customerInfo
      this.epmsPaymentInfo = epmsPaymentInfo
      this.epmsSubscriptionInfo = epmsSubscriptionInfo
      this.epmsPaymentSourceInfo = epmsPaymentSourceInfo

      if (transactionError) {
        this.transactionInfo = transactionError
      }

      if (subscriptionError) {
        this.subscriptionInfo = subscriptionError
      }

      if (customerError) {
        this.customerInfo = customerError
      }

      if (epmsPaymentError) {
        this.epmsPaymentInfo = epmsPaymentError
      }

      if (epmsSubscriptionError) {
        this.epmsSubscriptionInfo = epmsSubscriptionError
      }

      if (epmsPaymentSourceError) {
        this.epmsPaymentSourceInfo = epmsPaymentSourceError
      }

      this.epmsPaymentResponseData = null
    })

    this.epik.events[EpikEventName.TRANSACTION_STATUS_POLL].subscribe((event: EpikEvent) => {
      let transactionInfo = event.data.transactionInfo

      if (transactionInfo && transactionInfo.epayment_status !== TransactionStatus.PENDING) {
        this.transactionInfo = event.data.transactionInfo
      }
    })

    this.epik.events[EpikEventName.SEND_EPMS_PAYMENT_END].subscribe((event: EpikEvent) => {
      let {epmsPaymentResponseData, epmsPaymentResponseError} = event.data

      if (epmsPaymentResponseData) {
        this.epmsPaymentResponseData = epmsPaymentResponseData
      }

      if (epmsPaymentResponseError) {
        this.epmsPaymentResponseData = epmsPaymentResponseError
      }

      this.flags.sending = false
    })
  }

  showAddressFields() {
    let paymentMethods = [
      PaymentMethod.DIRECT_DEBIT,
      PaymentMethod.PAYMENT_SLIP,
      PaymentMethod.PAYMENT_SLIP_WITH_REFERENCE_NUMBER,
    ]

    return paymentMethods.indexOf(this.data.payment_method) !== -1 || isCreditCardPaymentMethod(this.data.payment_method)
  }

  private async submitForm() {
    if (this.flags.sending) {
      return
    }

    try {
      this.flags.sending = true
      this.validationErrors = null
      this.transactionInfo = null
      this.subscriptionInfo = null
      this.customerInfo = null
      this.epmsPaymentInfo = null
      this.epmsSubscriptionInfo = null
      this.epmsPaymentResponseData = null

      this.payment.markAllFieldsAsTouched()
      let errors = await this.payment.validate()

      if (errors) {
        this.validationErrors = errors
        this.flags.sending = false
      } else {
        await this.payment.send()
      }
    } catch (error) {
      this.flags.sending = false
      this.epmsPaymentResponseData = error.message
      console.error('[demo] error during payment validation/sending', error)
    }
  }

  private resetForm() {
    this.epik.transactionStatusPoll.stop()
    this.validationErrors = null
    this.epmsPaymentResponseData = null

    let params = reduce(AllEpikQueryParams, (result, v) => {
      result[v] = undefined

      return result
    }, {})

    this.epik.updateCurrentUrl({params})
  }

  render() {
    return (
      <div>
        <h1>{this.props.title}</h1>
        <form onSubmit={(e) => {e.preventDefault(); this.submitForm()}} autoComplete="off">

          <div className="row">
            <Checkbox name="test_mode" label="Test mode" data={this.data}/>
            <Checkbox name="credit_card_iframe" label="Credit card iframe" data={this.flags}/>
            <Checkbox name="return_parameters" label="Return parameters" data={this.data}/>
            <Checkbox name="skip_cvv" label="Skip cvv" data={this.flags}/>
            <Select name="language" label="Language" options={this.options.language} data={this.data}/>
          </div>

          <div className="row">
            <Select name="flow" label="Flow" options={this.options.flow} data={this.data}/>
            <Select name="payment_method" label="Payment method" options={this.options.payment_method} data={this.data}/>
            <Select name="payment_provider" label="Payment provider" options={this.options.payment_provider} data={this.data}/>
            <Select name="profile" label="Profile" options={this.options.profile} data={this.data}/>
            <Checkbox name="recurring" label="Recurring" data={this.data}/>

            {this.data.recurring && (
              <Select name="recurring_interval" label="Recurring interval" options={this.options.recurring_interval} data={this.data}/>
            )}
          </div>

          <div className="row">
            <Input name="amount" label="Amount" data={this.data}/>
            <Select name="currency" label="Currency" options={this.options.currency} data={this.data}/>
          </div>

          <div className="row bt">
            <Input name="stored_customer_firstname" label="First name" data={this.data}/>
            <Input name="stored_customer_lastname" label="Last name" data={this.data}/>
          </div>

          <div className="row">
            <Input name="stored_customer_email" label="E-mail" data={this.data}/>
            <Input name="stored_customer_birthdate" label="Birthdate" data={this.data}/>
            <Input name="stored_customer_phone" label="Phone" data={this.data}/>
          </div>

          {this.data.payment_provider === PaymentProvider.PAYU && (
            <div className="row">
              <Input name="stored_customer_document_type" label="Document Type" data={this.data}/>
              <Input name="stored_customer_document_number" label="Document Number" data={this.data}/>
              <Input name="stored_customer_company_identification_number" label="Company identification number" data={this.data}/>
            </div>
          )}

          <div className="row bt">
            <Checkbox name="stored_customer_donation_receipt" label="I want a donation receipt for tax returns" data={this.data}/>
          </div>

          {this.showAddressFields() && (
            <AddressFieldsView data={this.data} options={this.options}/>
          )}

          <PaymentSpecificFieldsView flags={this.flags} data={this.data} options={this.options}/>

          <CreditCardIframeView/>

          <div className="row bt">
            <button type="submit" disabled={this.flags.sending}>{this.flags.sending ? 'Sending...' : 'Send'}</button>
            <button type="button" onClick={this.resetForm}>Reset</button>
          </div>

          <DebugSection data={this.validationErrors} title="Validation Errors"/>
          <DebugSection data={this.transactionInfo} title="Transaction Info"/>
          <DebugSection data={this.subscriptionInfo} title="Subscription Info"/>
          <DebugSection data={this.customerInfo} title="Customer Info"/>
          <DebugSection data={this.epmsPaymentInfo} title="EPMS Payment Info"/>
          <DebugSection data={this.epmsSubscriptionInfo} title="EPMS Subscription Info"/>
          <DebugSection data={this.epmsPaymentSourceInfo} title="EPMS Payment Source Info"/>
          <DebugSection data={this.epmsPaymentResponseData} title="EPMS Payment Response Data/Error"/>
        </form>
      </div>
    )
  }
}

///////////////////////////////////////////////////////////////////////////////

export const runWidget = async (targetSelector: string, config: WidgetConfig): Promise<WidgetContainer> => {
  const widget = await createWidget(config)
  await renderWidget(widget, targetSelector)

  return widget
}

export const createWidget = async (config: WidgetConfig): Promise<WidgetContainer> => {
  const node = <Widget {...config}/>

  return {node}
}

export const renderWidget = async (widget: WidgetContainer, targetSelector: string) => {
  const target = document.querySelector(targetSelector)

  return new Promise((resolve) => {
    render(widget.node, target, () => {
      resolve()
    })
  })
}
