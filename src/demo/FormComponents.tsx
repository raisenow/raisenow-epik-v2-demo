import React, {Fragment} from 'react'
import {observer} from 'mobx-react'
import {isCreditCardPaymentMethod, PaymentMethod, PaymentProvider} from '@raisenow/epik'
import {DebugData, Input, Select} from './FormBaseComponents'
import {WidgetData, WidgetFlags, WidgetOptions} from './DemoWidget'

///////////////////////////////////////////////////////////////////////////////

type PaymentSpecificFieldsViewProps = {
  flags: WidgetFlags
  data: WidgetData
  options: WidgetOptions
}

export type AddressFieldsViewProps = {
  data: WidgetData
  options: WidgetOptions
}

///////////////////////////////////////////////////////////////////////////////

export function AddressFieldsView(props: AddressFieldsViewProps) {
  let {data, options} = props

  return (
    <Fragment>
      <div className="row bt">
        <Input name="stored_customer_street" label="Street" data={data}/>
        <Input name="stored_customer_street_number" label="Street Number" data={data}/>
      </div>
      <div className="row">
        <Input name="stored_customer_zip_code" label="Zip" data={data}/>
        <Input name="stored_customer_city" label="City" data={data}/>
      </div>
      <div className="row">
        <Select name="stored_customer_country" label="Country" options={options.country} data={data}/>
        <Input name="stored_customer_state" label="State" data={data}/>
      </div>
    </Fragment>
  )
}

export const PaymentSpecificFieldsView = observer((props: PaymentSpecificFieldsViewProps) => {
  let {data, options, flags} = props
  let paymentMethod = data.payment_method

  if (isCreditCardPaymentMethod(paymentMethod) && !flags.credit_card_iframe) {
    return (
      <Fragment>
        <div className="row bt">
          <Input name="cardno" label="Card number" data={data}/>
          <Input name="cvv" label="CVV" data={data}/>
        </div>
        <div className="row">
          <Input name="expm" label="Exp month" data={data}/>
          <Input name="expy" label="Exp year" data={data}/>
        </div>
      </Fragment>
    )
  }

  if (paymentMethod === PaymentMethod.CYMBA) { /*todo: check (api error - 500)*/ }

  if (paymentMethod === PaymentMethod.DIRECT_DEBIT) {
    return (
      <Fragment>
        <div className="row bt">
          <Input name="iban" label="IBAN" data={data}/>
          <Input name="bank_name" label="Bank Name" data={data}/>
        </div>
        <div className="row">
          <Input name="bank_zip_code" label="Bank Zip Code" data={data}/>
          <Input name="bank_city" label="Bank City" data={data}/>
        </div>
      </Fragment>
    )
  }

  if (paymentMethod === PaymentMethod.WEBBANKING_INDIA) { /*no additional fields*/ }

  if (paymentMethod === PaymentMethod.ELEKTRONISCHES_LASTSCHRIFTVERFAHREN) {
    return (
      <div className="row bt">
        <Input name="bankiban" label="IBAN" data={data}/>
      </div>
    )
  }

  if (paymentMethod === PaymentMethod.SEPA_ONE) {
    return (
      <Fragment>
        <div className="row bt">
          <Input name="bankiban" label="IBAN" data={data}/>
        </div>
        <div className="row">
          <Input name="bankbic" label="BIC" data={data}/>
        </div>
      </Fragment>
    )
  }

  if (paymentMethod === PaymentMethod.ELECTRONIC_PAYMENT_SERVICE) { /*no additional fields*/ }

  if (paymentMethod === PaymentMethod.MASTER_PASS) { /*todo: check (account needed)*/ }

  if (paymentMethod === PaymentMethod.PAYMENT_SLIP) {
    return (
      <div className="row bt">
        <Select name="stored_es_ordered" label="Delivery type" options={options.payment_slip_delivery_types} data={data}/>
      </div>
    )
  }

  if (paymentMethod === PaymentMethod.PAYMENT_SLIP_WITH_REFERENCE_NUMBER) {
    return (
      <div className="row bt">
        <Select name="stored_ezs_ordered" label="Delivery type" options={options.payment_slip_delivery_types} data={data}/>
      </div>
    )
  }

  if (paymentMethod === PaymentMethod.PAY_PAL) { /*todo: check (account needed)*/ }

  if (paymentMethod === PaymentMethod.POST_FINANCE) { /*todo: check (account needed)*/ }

  if (paymentMethod === PaymentMethod.POST_FINANCE_CARD) { /*no additional fields*/ }

  if (paymentMethod === PaymentMethod.SMS) {
    return (
      <div className="row bt">
        <Input name="msisdn" label="Msisdn" data={data}/>
      </div>
    )
  }

  if (paymentMethod === PaymentMethod.SOFORT) { /*todo: check (account needed)*/ }

  if (paymentMethod === PaymentMethod.TWINT) { /*no additional fields*/ }

  if (paymentMethod === PaymentMethod.MPOS) { /*no additional fields*/ }

  return null
})

export const CreditCardIframeView = () => <div className="credit-card-iframe"/>

export function DebugSection(props) {
  let {data, title} = props

  if (!data) {
    return null
  }

  return (
    <section className="debug-section">
      <b>{title}</b>
      <DebugData data={data}/>
    </section>
  )
}

///////////////////////////////////////////////////////////////////////////////
