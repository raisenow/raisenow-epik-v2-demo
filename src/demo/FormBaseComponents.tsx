import React, {SyntheticEvent} from 'react'
import {observer} from 'mobx-react'

///////////////////////////////////////////////////////////////////////////////

export type WidgetFormFieldValue = string | number | boolean

export type WidgetFormData = {
  [key: string]: WidgetFormFieldValue
}

export type WidgetFormElementProps = {
  data: WidgetFormData
  name: string
  label: string
}

export type InputProps = WidgetFormElementProps

export type CheckboxProps = WidgetFormElementProps

export type SelectProps = WidgetFormElementProps & {
  options?: SelectOption[]
  empty?: string
}

export type SelectOption = {
  value: WidgetFormFieldValue
  label: string
}

///////////////////////////////////////////////////////////////////////////////

export function handleInputChange(event: SyntheticEvent, data: WidgetFormData) {
  let target = event.target as HTMLInputElement
  let value = target.type === 'checkbox' ? target.checked : target.value
  let name = target.name

  data[name] = value
}

///////////////////////////////////////////////////////////////////////////////

export const Input = observer((props: InputProps) => {
  let {data, name, label} = props

  return (
    <label>
      {label}<br/>
      <input type="text"
             name={name}
             value={data[name] as string}
             placeholder={label}
             onChange={(e) => handleInputChange(e, data)}
             autoComplete="off"/>
    </label>
  )
})

export const Checkbox = observer((props: CheckboxProps) => {
  let {data, name, label} = props

  return (
    <label>
      <input type="checkbox"
             name={name}
             checked={!!data[name]}
             onChange={(e) => handleInputChange(e, data)}/>
      {label}
    </label>
  )
})

export const Select = observer((props: SelectProps) => {
  let {data, name, label, options, empty} = props

  return (
    <label>
      {label}<br/>
      <select name={name}
              value={data[name] as string}
              onChange={(e) => {handleInputChange(e, data)}}>
        {empty && <option value="">{empty}</option>}
        {options && options.map((v) =>
          <option key={v.value as string}
                  value={v.value as string}>
            {v.label}
          </option>
        )}
      </select>
    </label>
  )
})

///////////////////////////////////////////////////////////////////////////////

export const DebugData = ({data}) => (
  <pre className="debug-data">
    {JSON.stringify(data, null, 2)}
  </pre>
)

///////////////////////////////////////////////////////////////////////////////
