import React, {FC, Fragment} from 'react'
import {BasicExample} from './BasicExample'

///////////////////////////////////////////////////////////////////////////////

export const DemoPage: FC = () => (
  <Fragment>
    <div className="widget" id="widget1"/>
    <BasicExample/>
  </Fragment>
)
