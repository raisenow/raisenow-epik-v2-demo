import 'core-js/features/promise'
import 'core-js/features/symbol'
import 'core-js/features/object/assign'
import 'mobx-react/batchingForReactDom'
import React from 'react'
import {render} from 'react-dom'
import {createEpik, EpikConfig} from '@raisenow/epik'
import {DemoPage} from './DemoPage'
import {runWidget} from './DemoWidget'

///////////////////////////////////////////////////////////////////////////////

;(async () => {

  // @ts-ignore
  await import('./index.scss')

  const epikConfig: EpikConfig = {
    eppEnv: 'prod',
    eppMerchantId: 'srk-aced5e',
    // creditCardIframeUrl: 'iframe.html',            // epik simple iframe (local)
    creditCardIframeUrl: 'http://localhost:12345', // tamaro iframe (local)

    debug: true,
    transactionStatusPollForceRequestsCount: 5,
    epmsEnv: 'stage',
    // epmsAccountUuid: 'd71de4f6-e466-40dc-84ce-c1ce20b29b08', // raisenow? (twint)
    // epmsAccountUuid: 'c1f41b65-1aac-497b-b060-047efe859c0f', // formunauts? (payu)
    epmsAccountUuid: 'd8160c5d-6d11-4a34-b0f8-13cb55120cfb', // stripe
    // skipCvv: true,

    // eppApiKey: 'sal-rmz8x0',
    // eppMerchantId: 'sal-qfb3eu',
  };

  const epik1 = createEpik({
    ...epikConfig,
    creditCardIframeTargetSelector: '#widget1 .credit-card-iframe',
  });

  render(<DemoPage/>, document.getElementById('root'), () => {
    runWidget('#widget1', {
      epik: epik1,
      title: "Widget #1",
    });
  })

})()
