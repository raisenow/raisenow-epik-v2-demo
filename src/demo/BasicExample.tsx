import React, {FC, useEffect} from 'react'

///////////////////////////////////////////////////////////////////////////////

export const BasicExample: FC = () => {
  useEffect(() => {
    if (window['Prism']) {
      window['Prism'].highlightAll()
    }
  }, [])

  return (
    <div className="basic-example">
      <h1 className="width-border-top">Minimal widget implementation (example)</h1>
      <pre>
        <code className="language-javascript">
          {`
;(async () => {

  //////////////////////////////////////////////////////////////////////////////
  // 1. Create and initialize a new EPIK instance
  //////////////////////////////////////////////////////////////////////////////

  // Create EPIK instance
  let epik = rnw.epik.createEpik({
    eppApiKey: '1234567890',
  })

  // Subscribe to events
  epik.events.fetchPaymentDataEnd.subscribe((event) => {
    let transactionInfo = event.data.transactionInfo
    let success = transactionInfo && transactionInfo.epayment_status === 'success'

    if (success) {
      console.log('successful payment', transactionInfo)
    }

    if (!success) {
      console.error('failed payment', transactionInfo)
    }
  })

  // Initialize EPIK instance
  await epik.init()

  //////////////////////////////////////////////////////////////////////////////
  // 2. Create and send payment
  //////////////////////////////////////////////////////////////////////////////

  // Create a new payment object
  let payment = epik.createPayment({
    payment_method: 'cc',
    currency: 'usd',
    amount: 1000,
    cardno: '4242424242424242',
    cvv: '123',
    expm: '12',
    expy: '21',
    stored_customer_firstname: 'John',
    stored_customer_lastname: 'Snow',
    stored_customer_street: 'Wallstreet',
    stored_customer_street_number: '1227',
    stored_customer_zip_code: '1337',
    stored_customer_city: 'New York',
    stored_customer_country: 'CH',
  })
  
  // show cc-iframe
  await payment.showCreditCardIframe()
  
  // hide cc-iframe
  payment.hideCreditCardIframe()

  // Define sendPayment function
  let sendPayment = async (payment) => {
    // mark all fields in cc-iframe as touched
    // this must be run before payment.validate() if cc-iframe is used
    payment.markAllFieldsAsTouched()
    
    // Validate payment
    let errors = await payment.validate()

    if (errors) {
      // Handle validation errors if any
      console.log('validation errors', errors)
    } else {
      // Send payment
      await payment.send()
    }
  }

  // Send payment
  sendPayment(payment)

})()
          `}
        </code>
      </pre>
    </div>
  )
}
