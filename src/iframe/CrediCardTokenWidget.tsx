import React, {FC, useState} from 'react'
import {useEffect, useRef} from 'react'
import {render} from 'react-dom'
import {keys, pick} from 'lodash'
import qs from 'qs'
import {ResizeObserver} from 'resize-observer'
import {
  createEpik,
  CreditCardIframeMessageType,
  Epik,
  getCreditCardType,
  Messenger,
  PaymentConfig,
  PaymentFlow,
  PaymentMethod,
} from '@raisenow/epik'

///////////////////////////////////////////////////////////////////////////////

export type WidgetConfig = {}

export type WidgetProps = {
  epik: Epik
  messenger: Messenger
  flow: PaymentFlow
}

export type WidgetContainer = {
  node: JSX.Element
}

///////////////////////////////////////////////////////////////////////////////

export const Widget: FC<WidgetProps> = (props) => {
  let {epik, messenger, flow: initialFlow} = props
  let [flow, setFlow] = useState(initialFlow)
  let form: Partial<PaymentConfig> = {
    cardno: null,
    expm: null,
    expy: null,
    cvv: null,
    payment_method: PaymentMethod.CREDIT_CARD,
  }

  let skipCvv = epik.config.skipCvv
  let token = useRef<string>(null)

  useEffect(() => {
    messenger.on(CreditCardIframeMessageType.SET_FLOW, (flow) => {
      setFlow(flow)
    })

    messenger.respond(CreditCardIframeMessageType.TOKENIZE, async () => {
      if (token.current) {
        messenger.emit(CreditCardIframeMessageType.TOKENIZE_SUCCESS, token.current)

        return token.current
      }

      try {
        let {cardno, expm, expy, cvv} = form
        let newToken = await epik.getCreditCardToken({flow, cardno, expm, expy, cvv})

        messenger.emit(CreditCardIframeMessageType.TOKENIZE_SUCCESS, newToken)
        token.current = newToken

        return newToken

      } catch (err) {

        messenger.emit(CreditCardIframeMessageType.TOKENIZE_ERROR)
        token.current = null

        return null
      }
    })

    messenger.respond(CreditCardIframeMessageType.DETECT_CREDIT_CARD_TYPE, () => {
      return form.payment_method
    })

    messenger.respond(CreditCardIframeMessageType.VALIDATE, async () => {
      let payment = epik.createPayment()
      payment.update(form)

      let errors = await epik.validator.validateCreditCard(payment.read(), epik, null)
      let relevantErrors = pick(errors, keys(form))

      if (keys(relevantErrors).length > 0) {
        messenger.emit(CreditCardIframeMessageType.VALIDATE_ERROR, relevantErrors)
      } else {
        messenger.emit(CreditCardIframeMessageType.VALIDATE_SUCCESS)
      }

      return relevantErrors
    })
  }, [])

  let onChange = (e) => {
    // clear token on form data change
    token.current = null

    messenger.emit(CreditCardIframeMessageType.DATA_CHANGED)

    let formKey = e.target.name
    let formValue = e.target.value

    form[formKey] = formValue

    if (formKey === 'cardno') {
      let ccType = getCreditCardType(formValue, flow)
      ccType = ccType ? ccType : PaymentMethod.CREDIT_CARD

      if (form.payment_method !== ccType) {
        form.payment_method = ccType
        messenger.emit(CreditCardIframeMessageType.CREDIT_CARD_TYPE_CHANGED, ccType)
      }
    }
  }

  let submit = () => messenger.emit(CreditCardIframeMessageType.SUBMIT)

  return (
    <form onSubmit={(e) => {e.preventDefault(); submit()}}>
      <input type="text" name="cardno" placeholder="cardno" onChange={onChange}/>
      <input type="text" name="expm" placeholder="expm" onChange={onChange}/>
      <input type="text" name="expy" placeholder="expy" onChange={onChange}/>

      {!skipCvv && (
        <input type="text" name="cvv" placeholder="cvv" onChange={onChange}/>
      )}

      <input type="submit" style={{display: 'none'}}/>
    </form>
  )
}

///////////////////////////////////////////////////////////////////////////////

export const runWidget = async (targetSelector: string, config: WidgetConfig = {}): Promise<WidgetContainer> => {
  const widget = await createWidget(config)
  await renderWidget(widget, targetSelector)

  return widget
}

export const createWidget = async (config: WidgetConfig): Promise<WidgetContainer> => {
  let {channel, debug: debugParam, flow} = qs.parse(window.location.search, {ignoreQueryPrefix: true})
  const debug = debugParam === 'true'

  let messenger = new Messenger({
    listenTo: window,
    emitTo: window.parent,
    label: 'iframe',
    channel: channel as string,
    debug,
  })

  let el = document.body
  let getHeight = () => el.clientHeight

  // respond whenever parent asks for iframe content height
  messenger.respond(CreditCardIframeMessageType.HEIGHT, () => getHeight())

  let resizeObserver = new ResizeObserver(() => {
    messenger.emit(CreditCardIframeMessageType.HEIGHT, getHeight())
  })
  resizeObserver.observe(el)

  let epikConfig = await messenger.request(CreditCardIframeMessageType.EPIK_CONFIG)
  let epik = createEpik({...epikConfig, debug})

  const node = <Widget epik={epik} messenger={messenger} flow={flow as PaymentFlow}/>

  return {node}
}

export const renderWidget = async (widget: WidgetContainer, targetSelector: string) => {
  const target = document.querySelector(targetSelector)

  return new Promise((resolve) => {
    render(widget.node, target, () => {
      resolve()
    })
  })
}
