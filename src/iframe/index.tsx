import 'core-js/features/promise'
import 'core-js/features/symbol'
import 'core-js/features/object/assign'
import {runWidget} from './CrediCardTokenWidget'

///////////////////////////////////////////////////////////////////////////////

;(() => {
  runWidget('#root')
})()
