import {resolve} from 'path'
import {getIfUtils, removeEmpty} from 'webpack-config-utils'
import {Configuration, ConfigurationFactory, HashedModuleIdsPlugin, HotModuleReplacementPlugin, IgnorePlugin} from 'webpack'
import WebpackBarPlugin from 'webpackbar'
import {CleanWebpackPlugin} from 'clean-webpack-plugin'
import HtmlWebpackPlugin from 'html-webpack-plugin'
import ForkTsCheckerWebpackPlugin from 'fork-ts-checker-webpack-plugin'
import OptimizeCSSAssetsPlugin from 'optimize-css-assets-webpack-plugin'
import MiniCssExtractPlugin from 'mini-css-extract-plugin'
import safeParser from 'postcss-safe-parser'
import {BundleAnalyzerPlugin} from 'webpack-bundle-analyzer'

///////////////////////////////////////////////////////////////////////////////

const getConfig: ConfigurationFactory = env => {
  // "ifMin" function returns true if "--env.min" argument is passed
  // to webpack cli, otherwise it returns false
  // "ifNotMin" function – vice versa
  const {ifMin, ifNotMin} = getIfUtils(
    {min: env && (env as any)['min'] === true},
    ['min']
  )

  // NODE_ENV is being set according to "--env.min"
  process.env.NODE_ENV = ifMin('production', 'development')

  // Affects all chunks and assets URLs,
  // used in "output.publicPath" and "devServer.publicPath"
  const publicPath = ''

  // Reduce stats output verbosity
  const stats = {
    modules: false,
    children: false,
    chunks: false,
    env: true,
    entrypoints: true,
  }

  const htmlWebpackPluginOptions = {
    inject: false,
    minify: ifMin(
      {
        collapseWhitespace: true,
        removeComments: true,
        removeRedundantAttributes: true,
        removeScriptTypeAttributes: true,
        removeStyleLinkTypeAttributes: true,
        useShortDoctype: true,
        quoteCharacter: '"',
        minifyJS: true,
        minifyCSS: true,
      },
      false,
    )
  }

  const config: Configuration = {
    mode: ifMin('production', 'development'),
    resolve: {
      symlinks: false,
      extensions: ['.ts', '.tsx', '.js', '.jsx', '.json', '.scss'],
      modules: [resolve(__dirname, 'src'), 'node_modules'],
    },
    entry: {
      demo: './src/demo/index.tsx',
      iframe: './src/iframe/index.tsx',
    },
    // target: 'web', //?
    output: {
      publicPath,
      path: resolve(__dirname, 'dist'),
      filename: '[name].js',
      chunkFilename: ifMin('[name]-[contenthash:16].js', '[name].js'),
      crossOriginLoading: 'anonymous',
      // libraryTarget: 'umd', //?
    },
    stats,
    module: {
      rules: [
        {
          test: /\.(js|ts)x?$/,
          exclude: /node_modules/,
          loader: 'babel-loader',
        },
        {
          test: /\.s?css$/,
          use: removeEmpty([
            ifMin({loader: MiniCssExtractPlugin.loader}),
            ifNotMin({loader: 'style-loader'}),
            {loader: 'css-loader', options: {sourceMap: true, importLoaders: 3}},
            {loader: 'postcss-loader', options: {sourceMap: true}},
            {loader: 'resolve-url-loader', options: {sourceMap: true, keepQuery: true}},
            {loader: 'sass-loader', options: {sourceMap: true}},
          ]),
        },
        {
          test: /\.(png|jpe?g|gif|ico|svg|ttf|eot|woff2?)$/,
          loader: 'url-loader',
          options: {
            name: ifMin('[name]-[hash:16].[ext]', '[name].[ext]'),
            limit: 1000,
            esModule: false,
          },
        },
        {
          test: /\.html$/,
          use: [
            {loader: 'ejs-loader', options: {esModule: false}},
            {loader: 'extract-loader'},
            {loader: 'html-loader', options: {attributes: true}},
          ],
        },
      ],
    },
    devtool: ifMin(false, 'cheap-eval-source-map'), //?
    devServer: {
      publicPath: prepareDevServerPublicPath(publicPath),
      contentBase: false,
      historyApiFallback: {
        disableDotRule: true,
      },
      hot: true,
      open: false,
      stats,
    },
    optimization: ifMin({
      minimize: true,
      removeEmptyChunks: true,
    }),
    plugins: removeEmpty([
      new WebpackBarPlugin(),
      ifMin(new CleanWebpackPlugin({
        // dry: true,
        verbose: true,
      })),
      new HtmlWebpackPlugin({
        template: './src/demo/index.html',
        filename: 'index.html',
        inject: false,
        chunks: ['demo'],
      }),
      new HtmlWebpackPlugin({
        template: './src/iframe/index.html',
        filename: 'iframe.html',
        chunks: ['iframe'],
        ...htmlWebpackPluginOptions,
      }),
      new ForkTsCheckerWebpackPlugin({
        tsconfig: 'tsconfig.json',
        eslint: false,
        async: ifNotMin(),
        useTypescriptIncrementalApi: true,
        checkSyntacticErrors: true,
        reportFiles: ['./src/**/*'],
      }),
      ifNotMin(new HotModuleReplacementPlugin()),
      ifMin(new OptimizeCSSAssetsPlugin({
        cssProcessorOptions: {
          parser: safeParser,
          safe: true,
        },
      })),
      ifMin(new MiniCssExtractPlugin({
        filename: '[name]-[contenthash:16].css',
        chunkFilename: '[name]-[contenthash:16].css',
      })),
      // new ContextReplacementPlugin(/moment[\\\/]locale$/, /^\.\/(en|de|fr|it)$/),
      new IgnorePlugin({
        resourceRegExp: /^\.\/locale$/,
        contextRegExp: /moment$/,
      }),
      ifMin(new HashedModuleIdsPlugin()),
      // ifProd(new BundleAnalyzerPlugin()),
    ]),
  }

  return removeEmpty(config)
}

export default getConfig

///////////////////////////////////////////////////////////////////////////////

export const prepareDevServerPublicPath = (publicPath?: string): string => {
  let path = (publicPath || '/').trim().toLowerCase()

  if (
    !path.startsWith('/') &&
    !path.startsWith('http') &&
    !path.startsWith('https')
  ) {
    path = `/${path}`
  }

  if (!path.endsWith('/')) {
    path = `${path}/`
  }

  return path
}
